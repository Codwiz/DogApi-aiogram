from aiogram import Bot,Dispatcher,executor,types
from dog_api.dog import DogApi

bot = Bot(token='token')
dp = Dispatcher(bot=bot)

@dp.message_handler(commands=['dog'])
async def get_dog(message: types.Message):
    dog_image = await DogApi.get_dog()
    await bot.send_photo(chat_id=message.chat.id, photo=dog_image)


if __name__ == "__main__":
    executor.start_polling(dp, skip_updates=True)
