import aiohttp
from .exceptions import ApiError

class DogApi:

    DOG_API_URL = 'https://dog.ceo/api/breeds/image/random'

    @staticmethod
    async def validate_request(request: aiohttp.ClientResponse) -> bool:
        if request.status == 200:
            request_json = await request.json()
            if request_json.get('status') and request_json.get('message'):
                if request_json.get('status') == 'success':
                    return True
        return False
    
    @classmethod
    async def get_dog(cls):
        async with aiohttp.ClientSession() as session:
            async with session.get(cls.DOG_API_URL) as r_to_api:
                if await cls.validate_request(r_to_api):
                    request_json = await r_to_api.json()
                    img_url = request_json.get('message')
                    async with session.get(img_url) as img_request:
                        return await img_request.read()
                else:
                    raise ApiError()